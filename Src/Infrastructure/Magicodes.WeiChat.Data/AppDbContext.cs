﻿using Magicodes.WeiChat.Data.Migrations;
using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Data.Models.WeiChat;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magicodes.WeiChat.Data.Models.Interface;
using EntityFramework.DynamicFilters;
using Magicodes.Data.Multitenant;
using Magicodes.WeiChat.Data.Models.Log;
using Magicodes.WeiChat.Data.Models.Site;
using Magicodes.WeiChat.Data.Models.Settings;

namespace Magicodes.WeiChat.Data
{
    public partial class AppDbContext : MultitenantIdentityDbContext<AppUser, AppRole, string, int, AppUserLogin, AppUserRole, AppUserClaim>
    {
        static AppDbContext()
        {
            var type = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
            if (type == null)
                throw new Exception("Do not remove, ensures static reference to System.Data.Entity.SqlServer");

            //初始化时自动更新迁移到最新版本
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AppDbContext, Configuration>());
        }
        /// <summary>
        /// 初始化DbContext
        /// </summary>
        public AppDbContext()
            : base("DefaultConnection")
        {

        }
        public static AppDbContext Create()
        {
            return new AppDbContext();
        }
        /// <summary>
        /// 初始化DbContext
        /// </summary>
        /// <param name="strConnection"></param>
        public AppDbContext(string strConnection)
            : base(strConnection)
        {
        }
    }
}
