﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models.CMS
{
    /// <summary>
    /// 栏目管理
    /// </summary>
    public class Column_Manage: WeiChat_TenantBase<long>
    {
        /// <summary>
        /// 栏目名称
        /// </summary>
        [Display(Name = "栏目名称")]
        [Required]
        public string ColumName { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        [Display(Name = "简介")]
        public string Intro { get; set; }
        /// <summary>
        /// 发布人
        /// </summary>
        [Display(Name = "发布人")]
        public string Autor { get; set; }
        /// <summary>
        /// 是否单页
        /// </summary>
        [Display(Name = "是否单页")]
        public bool IsOnePage { get; set; }

        [Display(Name = "内容")]
        [DataType(DataType.MultilineText)]
        public string OnePageContent { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        [Display(Name = "是否显示")]
        public bool IsShow { get; set; }
    }
}
