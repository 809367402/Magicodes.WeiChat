﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models.Interface
{
    public interface IAdminCreate<TCreateByKey>
    {
        /// <summary>
        /// 创建人
        /// </summary>
        TCreateByKey CreateBy { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        DateTime CreateTime { get; set; }
    }
}
