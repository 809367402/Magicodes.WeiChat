﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : Log_Audit.cs
//          description :
//  
//          created by 李文强 at  2016/10/06 10:34
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub：https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.Log
{
    /// <summary>
    ///     审计日志
    /// </summary>
    public class Log_Audit : Log_AdminBase<long>
    {
        [Display(Name = "审计唯一编号")]
        public string Code { get; set; }

        [Display(Name = "备注")]
        [MaxLength(500)]
        public string Remark { get; set; }

        [Display(Name = "请求路径")]
        [MaxLength(500)]
        public string RequestUrl { get; set; }

        [Display(Name = "请求数据")]
        public string FormData { get; set; }

        [Display(Name = "标题")]
        public string Title { get; set; }

        [Display(Name = "执行时间")]
        public long ExecutionDuration { get; set; }

        [Display(Name = "客户端IP")]
        public string ClientIpAddress { get; set; }

        [Display(Name = "异常")]
        public string Exception { get; set; }

        [Display(Name = "浏览器信息")]
        public string BrowserInfo { get; set; }

        [Display(Name = "客户端电脑名称")]
        public string ClientName { get; set; }

        [Display(Name = "自定义数据")]
        public string CustomData { get; set; }

        [Display(Name = "是否处理成功")]
        public bool IsSuccess { get; set; }
    }
}