﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : Log_LoginSuccess.cs
//          description :
//  
//          created by 李文强 at  2016/10/06 10:34
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub：https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.Log
{
    /// <summary>
    ///     登录成功
    /// </summary>
    public class Log_LoginSuccess : Log_AdminBase<long>
    {
        [Display(Name = "登录名")]
        public string LoginName { get; set; }

        [Display(Name = "客户端IP")]
        public string ClientIpAddress { get; set; }

        [Display(Name = "浏览器信息")]
        public string BrowserInfo { get; set; }

        [Display(Name = "客户端电脑名称")]
        public string ClientName { get; set; }
    }
}