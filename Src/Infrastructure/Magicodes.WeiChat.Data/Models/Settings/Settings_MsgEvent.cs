﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models.Settings
{
    /// <summary>
    /// 服务器消息事件设置
    /// </summary>
    public class Settings_MsgEvent : WeiChat_AdminUniqueTenantBase<int>
    {
        [Display(Name = "是否启用事件转发")]
        public bool IsEnableForwarding { get; set; }

        [Display(Name = "微信事件转发地址")]
        public string WeichatForwardUrl { get; set; }

        [Display(Name = "商城事件转发地址")]
        public string ShopForwardUrl { get; set; }
    }
}
