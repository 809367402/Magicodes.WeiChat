﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : SiteResourceTypes.cs
//          description :
//  
//          created by 李文强 at  2016/10/06 10:42
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub：https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.Site
{
    /// <summary>
    ///     站点资源类型
    /// </summary>
    public enum SiteResourceTypes
    {
        /// <summary>
        ///     相册
        /// </summary>
        [Display(Name = "相册")] Gallery = 0,

        /// <summary>
        ///     语音
        /// </summary>
        [Display(Name = "语音")] Voice = 1,
        [Display(Name = "视频")] Video = 2,
        //[Display(Name = "缩略图")]
        //Thumb = 3,
        [Display(Name = "文章")] Article = 4,
        [Display(Name = "多图文")] News = 5
    }
}