﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : Site_Article.cs
//          description :
//  
//          created by 李文强 at  2016/10/06 10:42
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub：https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.Site
{
    /// <summary>
    ///     文章
    /// </summary>
    public class Site_Article : Site_FileBase
    {
        /// <summary>
        ///     内容
        /// </summary>
        [Display(Name = "内容")]
        [Required]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        [Display(Name = "作者")]
        public string Author { get; set; }

        [Display(Name = "摘要")]
        [MaxLength(500)]
        [DataType(DataType.MultilineText)]
        public string Summary { get; set; }

        [MaxLength(500)]
        [Display(Name = "链接")]
        public override string Url { get; set; }

        [MaxLength(500)]
        [Display(Name = "站内链接")]
        public override string SiteUrl { get; set; }

        [MaxLength(500)]
        [Display(Name = "原文链接")]
        public string OriginalUrl { get; set; }
    }
}