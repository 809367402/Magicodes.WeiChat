﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : Site_News.cs
//          description :
//  
//          created by 李文强 at  2016/10/06 10:42
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub：https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.Site
{
    /// <summary>
    ///     多图文
    /// </summary>
    public class Site_News : Site_FileBase
    {
        public Site_News()
        {
            Articles = new List<Site_NewsArticle>();
        }

        /// <summary>
        ///     文章列表
        /// </summary>
        public virtual ICollection<Site_NewsArticle> Articles { get; set; }

        /// <summary>
        ///     微信媒体Id
        /// </summary>
        [MaxLength(50)]
        public string MediaId { get; set; }

        [MaxLength(500)]
        [Display(Name = "封面")]
        public string FrontCoverImageUrl { get; set; }
    }
}