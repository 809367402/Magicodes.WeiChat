﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : Site_ResourceBase.cs
//          description :
//  
//          created by 李文强 at  2016/10/06 10:42
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub：https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System;

namespace Magicodes.WeiChat.Data.Models.Site
{
    public class Site_ResourceBase : WeiChat_TenantBase<Guid>
    {
        public Site_ResourceBase()
        {
            Id = Guid.NewGuid();
        }
    }
}