﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : Site_Video.cs
//          description :
//  
//          created by 李文强 at  2016/10/06 10:42
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub：https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.Site
{
    /// <summary>
    ///     视频
    /// </summary>
    public class Site_Video : Site_FileBase
    {
        /// <summary>
        ///     微信媒体Id
        /// </summary>
        [MaxLength(50)]
        public string MediaId { get; set; }
    }
}