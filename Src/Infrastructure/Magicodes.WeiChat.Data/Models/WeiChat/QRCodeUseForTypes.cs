﻿using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 二维码类型
    /// </summary>
    public enum QRCodeUseForTypes
    {
        /// <summary>
        /// 其他
        /// </summary>
        [Display(Name = "其他")]
        Orthers = 0,
        /// <summary>
        /// 绑定微信管理员
        /// </summary>
        [Display(Name = "绑定管理员")]
        BindManager = 1
    }
}