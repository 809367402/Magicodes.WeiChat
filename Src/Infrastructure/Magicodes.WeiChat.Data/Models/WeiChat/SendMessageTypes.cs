﻿using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 发送类型
    /// </summary>
    public enum SendMessageTypes
    {
        /// <summary>
        /// 文本
        /// </summary>
        [Display(Name = "文本")]
        Text = 0,
        /// <summary>
        /// 图片
        /// </summary>
        [Display(Name = "图片")]
        Image = 1,
        ///// <summary>
        ///// 音乐
        ///// </summary>
        //[Display(Name = "音乐")]
        //Music = 2,
        /// <summary>
        /// 语音
        /// </summary>
        [Display(Name = "语音")]
        Voice = 3,
        /// <summary>
        /// 视频
        /// </summary>
        [Display(Name = "视频")]
        Video = 4,
        /// <summary>
        /// 图文
        /// </summary>
        [Display(Name = "图文")]
        News = 5,
    }
}