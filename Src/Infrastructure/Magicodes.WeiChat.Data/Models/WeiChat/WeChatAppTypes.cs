﻿using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models
{
    /// <summary>
    /// 公众号类型
    /// </summary>
    public enum WeChatAppTypes
    {
        [Display(Name = "认证服务号")]
        Service = 0,
        [Display(Name = "认证订阅号")]
        Subscription = 1,
        [Display(Name = "企业号")]
        Enterprise = 2,
        [Display(Name = "测试号")]
        Test = 3
    }
}