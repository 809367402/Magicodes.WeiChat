﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Magicodes.WeChat.SDK;
using Magicodes.WeiChat.Data.Models.Interface;

namespace Magicodes.WeiChat.Data.Models
{
    /// <summary>
    /// 微信公众号配置
    /// </summary>
    public class WeiChat_App : IWeChatConfig, IAdminCreate<string>, IAdminUpdate<string>, ITenantId
    {
        /// <summary>
        /// AppId
        /// </summary>
        [MaxLength(50)]
        [Required]
        [Display(Name = "AppId")]
        public string AppId { get; set; }
        /// <summary>
        /// AppSecret
        /// </summary>
        [Required]
        [MaxLength(100)]
        [Display(Name = "AppSecret")]
        [DataType(DataType.Password)]
        public string AppSecret { get; set; }
        /// <summary>
        /// 微信号
        /// </summary>
        [MaxLength(20)]
        [Display(Name = "微信号")]
        public string WeiXinAccount { get; set; }
        /// <summary>
        /// 版权信息
        /// </summary>
        [MaxLength(20)]
        [Display(Name = "版权信息")]
        public string CopyrightInformation { get; set; }
        /// <summary>
        /// 客户信息
        /// </summary>
        [MaxLength(20)]
        [Display(Name = "客户信息")]
        public string CustomerInformation { get; set; }
        /// <summary>
        /// 填写服务器配置时必须，为了安全，请生成自己的Token。注意：正式公众号的Token只允许英文或数字的组合，长度为3-32字符
        /// </summary>
        [MaxLength(200)]
        public string Token { get; set; }


        [MaxLength(128)]
        [Display(Name = "创建人")]
        public string CreateBy { get; set; }
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        [MaxLength(128)]
        [Display(Name = "最后编辑")]
        public string UpdateBy { get; set; }
        [Display(Name = "更新时间")]
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 租户Id（使用租户Id作为Key）
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TenantId { get; set; }
        /// <summary>
        /// 公众号类型
        /// </summary>
        [Display(Name = "公众号类型")]
        public WeChatAppTypes WeChatAppType { get; set; }
    }
}