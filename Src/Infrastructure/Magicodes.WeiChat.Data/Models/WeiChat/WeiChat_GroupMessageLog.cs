﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 消息日志
    /// </summary>
    public class WeiChat_GroupMessageLog : WeiChat_TenantBase<long>
    {
        public string UserGroupId { get; set; }

        [NotMapped]
        public string WXName { get; set; }
        [NotMapped]
        public string OpenId { get; set; }
        [NotMapped]
        public bool IsUseOpenId { get; set; }

        public SexTypes SexType { get; set; }
        [Required]
        [MaxLength(2000)]
        public string MediaId { get; set; }
        [Required]
        [Display(Name = "类型")]
        public SendMessageTypes MessageType { get; set; }
        /// <summary>
        /// 是否发送成功
        /// </summary>
        [Display(Name = "是否发送成功")]
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
