﻿namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 微信图片回复关键字
    /// </summary>
    public class WeiChat_KeyWordImageContent : WeiChat_KeyWordContentTypeBase
    {
        /// <summary>
        /// 图片资源Id
        /// </summary>
        public string ImageMediaId { get; set; }
    }
}