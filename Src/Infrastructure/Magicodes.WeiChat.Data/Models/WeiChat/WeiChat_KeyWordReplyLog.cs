﻿using System;
using System.ComponentModel.DataAnnotations;
using Magicodes.WeiChat.Data.Models.Interface;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 回复日志
    /// </summary>
    public class WeiChat_KeyWordReplyLog : ITenantId
    {
        [Key]
        public long Id { get; set; }
        /// <summary>
        /// 接受的字符串
        /// </summary>
        [Display(Name = "用户发送内容")]
        public string ReceiveWords { get; set; }
        /// <summary>
        /// 命中的关键字
        /// </summary>
        [MaxLength(100)]
        [Display(Name = "命中的关键字")]
        public string KeyWord { get; set; }
        /// <summary>
        /// 自动回复Id
        /// </summary>
        [Display(Name = "自动回复配置")]
        public Guid? WeiChat_KeyWordAutoReplyId { get; set; }
        /// <summary>
        /// 内容Id
        /// </summary>
        public Guid? ContentId { get; set; }
        public int TenantId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }
        [Display(Name = "是否成功")]
        public bool IsSuccess { get; set; }
        [Display(Name = "异常日志")]
        public string Error { get; set; }
        [Display(Name = "来自")]
        [MaxLength(50)]
        public string From { get; set; }
        [Display(Name = "To")]
        [MaxLength(50)]
        public string To { get; set; }
        [Display(Name = "EventKey")]
        [MaxLength(200)]
        public string EventKey { get; set; }
        [Display(Name = "MsgId")]
        public long MsgId { get; set; }
    }
}