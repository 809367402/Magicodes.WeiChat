﻿using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 微信文本回复关键字
    /// </summary>
    public class WeiChat_KeyWordTextContent : WeiChat_KeyWordContentTypeBase
    {
        /// <summary>
        /// 内容
        /// </summary>
        [Display(Name = "文本内容")]
        [DataType(DataType.MultilineText)]
        public string Text { get; set; }
    }
}