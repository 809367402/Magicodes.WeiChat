﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Magicodes.WeiChat.Data.Models
{
    public class WeiChat_Material
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public DateTime UpdateTime { get; set; }
    }
}