﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 模板消息模板
    /// </summary>
    public class WeiChat_MessagesTemplate : WeiChat_TenantBase<int>
    {
        /// <summary>
        /// 模板编号
        /// </summary>
        [MaxLength(50)]
        [Display(Name = "模板编号")]
        public string TemplateNo { get; set; }
        [MaxLength(50)]
        [Display(Name = "模板库中模板的编号")]
        public string ShortNo { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        [Display(Name = "标题")]
        [Required]
        [MaxLength(200)]
        public string Title { get; set; }
        /// <summary>
        /// 一级行业
        /// </summary>
        [Display(Name = "一级行业")]
        [MaxLength(50)]
        public string OneIndustry { get; set; }
        /// <summary>
        /// 二级行业
        /// </summary>
        [Display(Name = "二级行业")]
        [MaxLength(50)]
        public string TwoIndustry { get; set; }
        /// <summary>
        /// 模板内容
        /// </summary>
        [Display(Name = "模板内容")]
        [MaxLength(500)]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }
        /// <summary>
        /// 示例内容
        /// </summary>
        [Display(Name = "示例内容")]
        [MaxLength(500)]
        [DataType(DataType.MultilineText)]
        public string Demo { get; set; }
        /// <summary>
        /// 是否允许测试
        /// </summary>
        public bool AllowTest { get; set; }
    }
}
