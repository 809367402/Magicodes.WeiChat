﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models.WeiChat
{
    /// <summary>
    /// 微信二维码
    /// </summary>
    public class WeiChat_QRCode : WeiChat_TenantBase<int>
    {
        /// <summary>
        /// 参数值
        /// </summary>
        [Display(Name = "参数值")]
        [Required]
        public string ParamsValue { get; set; }
        /// <summary>
        /// 过期时间（秒）
        /// </summary>
        [Display(Name = "过期时间（秒）")]
        [Required]
        public int ExpireSeconds { get; set; }
        /// <summary>
        /// 有效时间
        /// </summary>
        [Display(Name = "有效时间")]
        public DateTime? ExpireTime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Display(Name = "备注")]
        [MaxLength(50)]
        public string Remark { get; set; }
        /// <summary>
        /// 获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码。
        /// </summary>
        [MaxLength(100)]
        public string Ticket { get; set; }
        /// <summary>
        /// 用于
        /// </summary>
        [Display(Name = "用于")]
        public QRCodeUseForTypes UserFor { get; set; }
    }
}
