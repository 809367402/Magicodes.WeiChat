﻿using System;
using System.ComponentModel.DataAnnotations;
using Magicodes.WeiChat.Data.Models.Interface;

namespace Magicodes.WeiChat.Data.Models
{
    /// <summary>
    /// 同步日志
    /// </summary>
    public class WeiChat_SyncLog : ITenantId, IAdminCreate<string>
    {
        public WeiChat_SyncLog()
        {
            CreateTime = DateTime.Now;
        }
        /// <summary>
        /// 主键Id
        /// </summary>
        [Key]
        [Display(Name = "主键Id")]
        public int Id { get; set; }
        public WeiChat_SyncTypes Type { get; set; }
        public int TenantId { get; set; }
        /// <summary>
        /// 是否用户手动同步
        /// </summary>
        public bool IsUserSync { get; set; }
        [MaxLength(128)]
        [Display(Name = "创建人")]
        public string CreateBy { get; set; }
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }
        [MaxLength(2000)]
        public string Message { get; set; }
        [Display(Name = "是否成功")]
        public bool Success { get; set; }
    }
}