﻿namespace Magicodes.WeiChat.Data.Models
{
    public enum WeiChat_SyncTypes
    {
        /// <summary>
        /// 微信用户
        /// </summary>
        SyncWeChatUsersTask = 0,
        /// <summary>
        /// 多客服信息
        /// </summary>
        SyncMKFTask = 1,
        /// <summary>
        /// 图片资源
        /// </summary>
        SyncWeChatImagesTask = 2,
        /// <summary>
        /// 同步用户组
        /// </summary>
        SyncWeChatUserGroupTask = 3,
        /// <summary>
        /// 同步模板消息
        /// </summary>
        SyncMessagesTemplatesTask = 4
    }
}