﻿using Magicodes.WeiChat.Data.Models.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Data.Models
{
    public abstract class WeiChat_WeChatWithNoKeyBase : ITenantId
    {
        public virtual string OpenId { get; set; }

        public int TenantId { get; set; }
        public virtual DateTime CreateTime { get; set; }


    }
    public abstract class WeiChat_WeChatBase<TKey> : WeiChat_WeChatWithNoKeyBase
    {
        [Key]
        public virtual TKey Id { get; set; }
    }
}
