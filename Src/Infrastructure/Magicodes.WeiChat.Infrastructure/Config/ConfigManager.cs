﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//======================================================================
//
//        Copyright (C) 2014-2016 Magicodes.WeiChat    
//        All rights reserved
//
//        filename :ConfigManager
//        description :
//
//        created by 雪雁 at  2015/7/10 15:45:04
//        http://www.magicodes.net
//
//======================================================================
namespace Magicodes.WeiChat.Infrastructure.Config
{
    /// <summary>
    /// 配置管理器
    /// </summary>
    public class ConfigManager
    {
        private static object _locker = new object();//锁对象
        protected ConcurrentDictionary<Type, ConfigBase> Configs = new ConcurrentDictionary<Type, ConfigBase>();
        public T Load<T>()
        {
            //return SerializeHelper.Load<T>(path);
            throw new NotSupportedException();
        }

        public void Save<T>(T configType) where T : ConfigBase
        {
            throw new NotSupportedException();
        }

        public T Get<T>() where T : ConfigBase
        {
            lock (_locker)
            {
                var type = typeof(T);
                if (Configs.ContainsKey(type))
                {
                    return Configs[type] as T;
                }
                else
                {
                    var config = Load<T>();
                    if (config != null)
                    {
                        Configs.AddOrUpdate(type, config, (tKey, existingVal) => { return config; });
                        return config;
                    }
                }
            }
            return default(T);
        }

    }
}
