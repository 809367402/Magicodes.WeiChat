﻿namespace Magicodes.WeiChat.Infrastructure.Events
{
    /// <summary>
    /// 模板消息值定义
    /// </summary>
    public class MessageTemplateValue
    {
        /// <summary>
        /// 颜色，为空则使用默认值
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// 文本、变量、函数
        ///     文本：您收到一条订单信息
        ///     变量：{OrderInfo.Id}
        ///     函数：=GetOrderInfoById({OrderId})
        /// </summary>
        public string Expression { get; set; }
    }
}
