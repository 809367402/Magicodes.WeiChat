﻿using Magicodes.WeiChat.Data.Models.WeiChat;
using System.Collections.Generic;

namespace Magicodes.WeiChat.Infrastructure.Events
{
    /// <summary>
    /// 模板消息处理
    /// </summary>
    public class MessagesTemplateAction : EventActionBase
    {
        /// <summary>
        /// 模板消息Id
        /// </summary>
        public string MessagesTemplateId { get; set; }


        /// <summary>
        /// Key:TemplateDataItemKey
        /// Value:Variable or function
        /// </summary>
        public Dictionary<string, MessageTemplateValue> Items { get; set; }

        /// <summary>
        /// 接收者
        /// </summary>
        public string Receivers { get; set; }

        /// <summary>
        /// 链接
        /// </summary>
        public string Url { get; set; }
    }
}
