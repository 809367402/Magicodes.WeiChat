﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Infrastructure.Events
{
    /// <summary>
    /// 事件侦听
    /// </summary>
    public class EventListener
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 显示名称
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// 数据项
        /// </summary>
        public IEventData Data { get; set; }

        /// <summary>
        /// 处理
        /// </summary>
        public List<EventActionBase> Actions { get; set; }
    }
}
