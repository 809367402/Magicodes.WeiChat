﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magicodes.WeiChat.Infrastructure.Events
{
    /// <summary>
    /// 事件参数接口（触发时传递的参数类必须实现此接口）
    /// </summary>
    public interface IEventData
    {
    }
}
