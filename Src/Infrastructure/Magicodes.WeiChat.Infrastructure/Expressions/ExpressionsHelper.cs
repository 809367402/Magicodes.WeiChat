﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Z.Expressions;

namespace Magicodes.WeiChat.Infrastructure.Expressions
{
    public static class ExpressionsHelper
    {
        /// <summary>
        /// 执行C#字符串表达式
        /// </summary>
        /// <param name="express"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static string ExecuteEvalString(string express, params object[] parameters)
        {
            if (string.IsNullOrWhiteSpace(express)) return string.Empty;
            express = express.Trim();
            if (express.StartsWith("{") && express.EndsWith("}"))
            {
                express = express.Substring(1, express.Length - 2);
                return Eval.Execute<string>(express, parameters);
            }
            return express;
        }
    }
}
