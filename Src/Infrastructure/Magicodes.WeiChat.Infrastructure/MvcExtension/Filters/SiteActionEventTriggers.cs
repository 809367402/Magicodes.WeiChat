﻿namespace Magicodes.WeiChat.Infrastructure.MvcExtension.Filters
{
    /// <summary>
    /// 事件触发器
    /// </summary>
    public enum SiteActionEventTriggers
    {
        /// <summary>
        /// 操作执行前
        /// </summary>
        OnActionExecuting = 1,

        /// <summary>
        /// 操作执行后
        /// </summary>
        OnActionExecuted = 0
    }
}
