﻿namespace Magicodes.WeiChat.Infrastructure.WebApiExtension.Results
{
    public enum ApiTypes
    {
        Get = 0,
        Post = 1,
        Put = 2,
        Delete = 3
    }
}