﻿using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Data.Models;
using Magicodes.WeChat.SDK;
using Magicodes.WeiChat.Infrastructure;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using Magicodes.WeiChat.Helpers;

namespace Magicodes.WeiChat.Areas.App.Controllers
{
    [AllowAnonymous]
    public class AppBaseController : Controller
    {
        internal const string TenantIdSessionName = "Magicodes.TenantId";
        protected AppDbContext db = new AppDbContext();
        /// <summary>
        /// 租户Id
        /// </summary>
        public int TenantId
        {
            get
            {
                //请求参数中的租户Id
                var reqTennantId = default(int);
                #region 获取请求参数中的租户Id
                if (!string.IsNullOrWhiteSpace(Request.QueryString["TenantId"]))
                {
                    reqTennantId = Convert.ToInt32(Request.QueryString["TenantId"]);
                }
                else
                {
                    reqTennantId = Request.RequestContext.RouteData.Values["TenantId"] != null ? Convert.ToInt32(Request.RequestContext.RouteData.Values["TenantId"]) : default(int);
                }
                #endregion

                if (reqTennantId != default(int))
                {
                    HttpContext.Session[TenantIdSessionName] = reqTennantId;
                    return reqTennantId;
                }
                else if (HttpContext.Session[TenantIdSessionName] != null)
                {
                    return Convert.ToInt32(HttpContext.Session[TenantIdSessionName]);
                }
                return default(int);
            }
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //设置配置Key
            base.OnActionExecuting(filterContext);
        }
        protected override void OnAuthentication(AuthenticationContext filterContext)
        {
            //设置配置Key
            base.OnAuthentication(filterContext);
        }
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }
        protected override void OnException(ExceptionContext filterContext)
        {
            LogManager.GetCurrentClassLogger().Error(filterContext.Exception);
            filterContext.Result = ErrorTip(title: "出错啦！", message: "服务器出现错误，请稍后再试或联系客服！", okUrl: Url.TenantAction("WeChatOAuthTest", "AppDemo"));
            filterContext.ExceptionHandled = true;

        }
        /// <summary>
        /// 微信用户信息
        /// </summary>
        public WeiChat_User WeiChatUser { get { return WeiChatApplicationContext.Current.WeiChatUser; } }

        public ActionResult SuccessTip(string title = "成功标题", string message = "成功文本", string iconCls = "aui-icon-check", string okText = "确定", string okUrl = "", string okIconcls = "aui-icon-roundcheck", string cancelText = "取消", string cancelUrl = "", string canceIconcls = "aui-icon-roundclose", string titleCls = "aui-text-success")
        {
            ViewBag.Title = title;
            ViewBag.Message = message;
            ViewBag.IconCls = iconCls;
            ViewBag.TitleCls = titleCls;

            ViewBag.OkText = okText;
            ViewBag.OkUrl = okUrl;
            ViewBag.OkIconcls = okIconcls;

            ViewBag.CancelText = cancelText;
            ViewBag.CancelUrl = cancelUrl;
            ViewBag.CanceIconcls = canceIconcls;
            return View("~/Areas/App/Views/AppBase/Tip.cshtml");
        }
        public ActionResult ErrorTip(string title = "错误标题", string message = "错误文本", string iconCls = "aui-icon-close", string okText = "确定", string okUrl = "", string okIconcls = "aui-icon-roundcheck", string cancelText = "取消", string cancelUrl = "", string canceIconcls = "aui-icon-roundclose", string titleCls = "aui-text-danger")
        {
            ViewBag.Title = title;
            ViewBag.Message = message;
            ViewBag.IconCls = iconCls;
            ViewBag.TitleCls = titleCls;

            ViewBag.OkText = okText;
            ViewBag.OkUrl = okUrl;
            ViewBag.OkIconcls = okIconcls;

            ViewBag.CancelText = cancelText;
            ViewBag.CancelUrl = cancelUrl;
            ViewBag.CanceIconcls = canceIconcls;
            return View("~/Areas/App/Views/AppBase/Tip.cshtml");
        }
        public ActionResult WarnTip(string title = "警告标题", string message = "警告文本", string iconCls = "aui-icon-warn", string okText = "确定", string okUrl = "", string okIconcls = "aui-icon-roundcheck", string cancelText = "取消", string cancelUrl = "", string canceIconcls = "aui-icon-roundclose", string titleCls = "aui-text-warning")
        {
            ViewBag.Title = title;
            ViewBag.Message = message;
            ViewBag.IconCls = iconCls;
            ViewBag.TitleCls = titleCls;

            ViewBag.OkText = okText;
            ViewBag.OkUrl = okUrl;
            ViewBag.OkIconcls = okIconcls;

            ViewBag.CancelText = cancelText;
            ViewBag.CancelUrl = cancelUrl;
            ViewBag.CanceIconcls = canceIconcls;
            return View("~/Areas/App/Views/AppBase/Tip.cshtml");
        }
        public ActionResult ImageTip(string imageUrl = "/Images/WeChat/winner.png", string okText = "确定", string okUrl = "", string okIconcls = "aui-icon-roundcheck")
        {
            ViewBag.ImageUrl = imageUrl;
            ViewBag.OkText = okText;
            ViewBag.OkUrl = okUrl;
            ViewBag.OkIconcls = okIconcls;
            return View("~/Areas/App/Views/AppBase/ImageTip.cshtml");
        }
    }
}