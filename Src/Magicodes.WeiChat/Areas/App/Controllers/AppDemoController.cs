﻿using Magicodes.WeiChat.Infrastructure.MvcExtension.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Magicodes.WeiChat.Infrastructure;
using Newtonsoft.Json;
using NLog;
using Magicodes.WeChat.SDK;
using Magicodes.WeChat.SDK.Pays;
using Magicodes.WeChat.SDK.Pays.TenPayV3;
using Magicodes.WeiChat.Helpers;

namespace Magicodes.WeiChat.Areas.App.Controllers
{
    [RouteArea("App")]
    //注意继承自：AppBaseController
    public class AppDemoController : AppBaseController
    {
        //产品宣传
        public ActionResult ProductPromotion()
        {
            return View();
        }
        // GET: App/AppDemo/WeChatOAuthTest
        //博客说明：http://www.cnblogs.com/codelove/p/5355514.html
        [WeChatOAuth]
        public ActionResult WeChatOAuthTest()
        {
            return View(WeiChatUser);
        }

        #region 微相册
        //相册
        public ActionResult PhotoGallery()
        {
            return View();
        }
        //照片
        [Route("AppDemo/PhotoGallery/{typeId}/Photos", Name = "PhotoGallery_Photos_Route")]
        public ActionResult PhotoGallery_Photos(string typeId)
        {
            return View();
        }
        [Route("AppDemo/PhotoGallery/{typeId}/Upload", Name = "PhotoGallery_UploadPhotos_Route")]
        public ActionResult PhotoGallery_UploadPhotos(string typeId)
        {
            return View();
        }
        #endregion
        public ActionResult RoadMap()
        {
            return View();
        }
        //个人中心
        [WeChatOAuth]
        public ActionResult PersonalCenter()
        {
            return View(WeiChatUser);
        }
        //绑定手机
        [WeChatOAuth]
        public ActionResult PhoneBound()
        {
            return View(WeiChatUser);
        }
        [WeChatOAuth]
        public ActionResult TestPay()
        {
            #region 统一下单
            var model = new UnifiedorderRequest
            {
                OpenId = WeiChatApplicationContext.Current.WeiChatUser.OpenId,
                SpbillCreateIp = "8.8.8.8",
                OutTradeNo = PayUtil.GenerateOutTradeNo(),
                TotalFee = "1",
                NonceStr = PayUtil.GetNoncestr(),
                TradeType = "JSAPI",
                Body = "Magicodes.WeiChat--购买商品",
                DeviceInfo = "WEB"
            };
            var result = WeChatApisContext.Current.TenPayV3Api.Unifiedorder(model);

            var dict = new Dictionary<string, string>
            {
                {"appId", result.AppId},
                {"timeStamp", PayUtil.GetTimestamp()},
                {"nonceStr", result.NonceStr},
                {"package", "prepay_id=" + result.PrepayId},
                {"signType", "MD5"}
            };
            dict.Add("paySign", PayUtil.CreateMd5Sign(dict, WeChatConfigManager.Current.GetPayConfig().TenPayKey));
            #endregion
            ViewBag.PayPam = JsonConvert.SerializeObject(dict);
            return View();
        }
        public ActionResult ExceptionTest()
        {
            throw new Exception("异常测试！");
        }
        public ActionResult SuccessMessage()
        {
            return SuccessTip(title: "温馨提示", message: "您的认证申请提交成功，管理员会在1~2个工作日内完成审批，请耐心等待！", okUrl: Url.TenantAction("WeChatOAuthTest", "AppDemo"));
        }
        public ActionResult WarnMessage()
        {
            return WarnTip(title: "温馨提示", message: "您提交的资料存在问题，请确认！", okUrl: Url.TenantAction("WeChatOAuthTest", "AppDemo"));
        }
        /// <summary>
        /// 购物车
        /// </summary>
        /// <returns></returns>
        [WeChatOAuth]
        public ActionResult ShoppingCart()
        {
            return View();
        }

        [HttpGet]
        public ActionResult JSSDK()
        {
            return View();
        }
    }
}