﻿using Magicodes.WeiChat.Data.Models.Site;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Magicodes.WeiChat.Areas.App.Models
{
    public class PhotosAlbum
    {
        public Guid Id { get; set; }
        public SiteResourceTypes ResourceType { get; set; }
        public bool IsSystemResource { get; set; }
        public string Title { get; set; }
        public string SiteUrl { get; set; }
    }
}