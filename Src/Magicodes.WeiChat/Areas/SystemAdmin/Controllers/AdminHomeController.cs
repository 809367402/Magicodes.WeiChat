﻿using Magicodes.Mvc.RoleMenuFilter;
using Magicodes.WeiChat.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Magicodes.WeiChat.Areas.SystemAdmin.Controllers
{
    [RoleMenuFilter("主页", "F49C8230-9706-4CDB-9465-8F77ED505A9D", "Admin", iconCls: "fa fa-th-large",
         tag: "System", orderNo: 1)]
    public class AdminHomeController : SystemAdminBase<WeiChat_App, int>
    {
        // GET: SystemAdmin/AdminHome
        [RoleMenuFilter("系统主页", "82EA0F37-5C61-4B52-9ED5-B97D2ADE112F", "Admin", url: "/SystemAdmin/AdminHome",
             parentId: "F49C8230-9706-4CDB-9465-8F77ED505A9D", tag: "System")]
        public ActionResult Index()
        {
            return View();
        }
    }
}