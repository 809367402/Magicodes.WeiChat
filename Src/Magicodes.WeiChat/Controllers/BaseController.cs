﻿using Magicodes.WeiChat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Magicodes.WeiChat.Data;
using Microsoft.AspNet.Identity;
using System.Text;
using Magicodes.WeiChat.Data.Models.Interface;
using Magicodes.WeiChat.Infrastructure.MvcExtension.Results;
using Magicodes.WeChat.SDK;
using System.Data.Entity;
using Magicodes.WeiChat.Infrastructure;

namespace Magicodes.WeiChat.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        protected AppDbContext db = new AppDbContext();
        protected Lazy<string> userId;
        protected Lazy<string> userName;
        protected Lazy<int> userIenantId;
        protected Lazy<bool> hasConfigWeiChat;
        public BaseController()
        {
            userId = new Lazy<string>(() => User.Identity.GetUserId());
            userName = new Lazy<string>(() => User.Identity.GetUserName());
            userIenantId = new Lazy<int>(() => db.Users.Find(UserId).TenantId);
            hasConfigWeiChat = new Lazy<bool>(() => db.WeiChat_Apps.Any(p => p.TenantId == TenantId));
        }
        /// <summary>
        /// 访问凭据
        /// </summary>
        protected string AccessToken
        {
            get
            {
                return WeChatConfigManager.Current.GetAccessToken();
            }
        }
        /// <summary>
        /// 使用JSON.NET序列化
        /// </summary>
        /// <param name="data"></param>
        /// <param name="contentType"></param>
        /// <param name="contentEncoding"></param>
        /// <param name="behavior"></param>
        /// <returns></returns>
        protected override JsonResult Json(object data, string contentType,
                  Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonNetResult()
            {
                Data = data,
                JsonRequestBehavior = behavior,
                ContentType = contentType,
                ContentEncoding = contentEncoding
            };
        }
        /// <summary>
        /// 是否已配置微信信息
        /// </summary>
        public bool HasConfigWeiChat
        {
            get
            {
                return hasConfigWeiChat.Value;
            }
        }
        /// <summary>
        /// 获取当前用户Id
        /// </summary>
        public string UserId
        {
            get
            {
                return userId.Value;
            }
        }
        /// <summary>
        /// 当前用户名
        /// </summary>
        public string UserName
        {
            get
            {
                return userName.Value;
            }
        }
        /// <summary>
        /// 租户Id
        /// </summary>
        public int UserTenantId
        {
            get
            {
                return userIenantId.Value;
            }
        }
        /// <summary>
        /// 当前用户是否为系统租户
        /// </summary>
        public bool IsSystemIenant
        {
            get { return db.Account_Tenants.Any(p => p.Id == UserTenantId && p.IsSystemTenant); }
        }
        /// <summary>
        /// 应用Id
        /// </summary>
        public int AppId
        {
            get { return TenantId; }
        }

        /// <summary>
        /// 租户Id（如果是系统租户则能获取到参数中的租户Id）
        /// </summary>
        public int TenantId
        {
            get
            {
                return WeiChatApplicationContext.Current.TenantId;
            }
        }

        protected bool SetModelWithChangeStates<TModel, Tkey>(TModel model, Tkey key)
            where TModel : class, IAdminCreate<string>, IAdminUpdate<string>, ITenantId, new()
        {
            var isAdd = SetModel(model, key);
            if (isAdd)
            {
                db.Set<TModel>().Add(model);
            }
            else
            {
                db.Entry(model).State = EntityState.Modified;
            }
            return isAdd;
        }
        protected bool SetModelWithSaveChanges<TModel, Tkey>(TModel model, Tkey key)
            where TModel : class, IAdminCreate<string>, IAdminUpdate<string>, ITenantId, new()
        {
            var isAdd = SetModelWithChangeStates(model, key);
            db.SaveChanges();
            return isAdd;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="Tkey"></typeparam>
        /// <param name="model"></param>
        /// <param name="key"></param>
        /// <returns>是否为添加</returns>
        protected bool SetModel<TModel, Tkey>(TModel model, Tkey key)
          where TModel : class, IAdminCreate<string>, IAdminUpdate<string>, ITenantId, new()
        {
            //判断是否为默认值
            if (EqualityComparer<Tkey>.Default.Equals(key, default(Tkey)))
            {
                model.CreateBy = UserId;
                model.CreateTime = DateTime.Now;
                model.TenantId = TenantId;
                return true;
            }
            else
            {
                db.Set<TModel>().Attach(model);
                //取数据库值
                var databaseValues = db.Entry(model).GetDatabaseValues();
                model.CreateBy = databaseValues.GetValue<string>("CreateBy");
                model.CreateTime = databaseValues.GetValue<DateTime>("CreateTime");
                model.TenantId = databaseValues.GetValue<int>("TenantId");
                model.UpdateTime = DateTime.Now;
                model.UpdateBy = UserId;
                return false;
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}