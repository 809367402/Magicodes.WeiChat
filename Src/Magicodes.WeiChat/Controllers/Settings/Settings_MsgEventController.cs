﻿// ======================================================================
//  
//          Copyright (C) 2016-2020 湖南心莱信息科技有限公司    
//          All rights reserved
//  
//          filename : Settings_PointController.cs
//          description :
//  
//          created by 李文强 at  2016/09/26 17:13
//          Blog：http://www.cnblogs.com/codelove/
//          GitHub ： https://github.com/xin-lai
//          Home：http://xin-lai.com
//  
// ======================================================================

using System.Web.Mvc;
using Magicodes.WeiChat.Data.Models.Settings;
using Magicodes.Mvc.AuditFilter;
using Magicodes.Mvc.RoleMenuFilter;
using Magicodes.WeiChat.Helpers;

namespace Magicodes.WeiChat.Controllers.Settings
{
    public class Settings_MsgEventController : AdminUniqueTenantBaseController<Settings_MsgEvent>
    {
        [RoleMenuFilter("服务器事件设置", "{8EE0F68A-2B09-4FFE-92B5-191E2A649657}", "Admin,TenantManager,ShopManager",
             url: "/Settings_MsgEvent", parentId: "72A9DBB1-4982-407E-9A78-31DEB153AB24")]
        // GET: Settings_Point
        [AuditFilter("服务器事件设置", "{8EE0F68A-2B09-4FFE-92B5-191E2A649657}")]
        public override ActionResult Index()
        {
            return base.Index();
        }

        [HttpPost]
        public override ActionResult Index(Settings_MsgEvent model)
        {
            if (!string.IsNullOrWhiteSpace(model.ShopForwardUrl))
            {
                var tenantId = TenantId;
                if (!NotifyHelper.NotifyUrls.ContainsKey(tenantId))
                    NotifyHelper.NotifyUrls.Add(TenantId, model.ShopForwardUrl);
                else
                    NotifyHelper.NotifyUrls[tenantId] = model.ShopForwardUrl;
            }
            return base.Index(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
    }
}