﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Magicodes.WeiChat.Infrastructure.MvcExtension.Ajax;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Webdiyer.WebControls.Mvc;
using Magicodes.Mvc.AuditFilter;
using Magicodes.Mvc.RoleMenuFilter;
using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Data.Models.Log;

namespace Magicodes.WeiChat.Controllers.WeChat
{
    [RoleMenuFilter("日志监控", "F348A29F-9F4C-476A-9932-23C607A4F9FB", "Admin",
         iconCls: "fa fa-code")]
    public class Log_AccessController : TenantBaseController<Log_Access>
    {

        // GET: Log_Access
        [AuditFilter("访问日志列表", "790e3aac-ea8e-4373-9daa-7887ed5b7a2b")]
        [RoleMenuFilter("访问日志", "790e3aac-ea8e-4373-9daa-7887ed5b7a2b", "Admin",
             url: "/Log_Access", parentId: "F348A29F-9F4C-476A-9932-23C607A4F9FB")]
        public ActionResult Index(string q, int pageIndex = 1, int pageSize = 10)
        {
            var queryable = db.Log_Access.AsQueryable();
            if (!string.IsNullOrWhiteSpace(q))
            {
                //请替换为相应的搜索逻辑
                queryable = queryable.Where(p => p.RequestUrl.Contains(q) || p.ClientIpAddress.Contains(q) || p.BrowserInfo.Contains(q) || p.HttpMethod.Contains(q) || p.OpenId.Contains(q));
            }
            var pagedList = new PagedList<Log_Access>(
                             queryable.OrderByDescending(p => p.CreateTime)
                             .Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList(),
                             pageIndex, pageSize, queryable.Count());
            return View(pagedList);
        }

        // GET: Log_Access/Details/5
        [AuditFilter("访问日志详细", "59f80de1-8fe9-4d3e-b687-d3fb6b3d5d8d")]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Log_Access log_Access = db.Log_Access.Find(id);
            if (log_Access == null)
            {
                return HttpNotFound();
            }
            return View(log_Access);
        }

        // GET: Log_Access/Delete/5
        [AuditFilter("访问日志待删除", "50e679be-61a7-4218-8c8e-5dc3d327bb08")]
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Log_Access log_Access = db.Log_Access.Find(id);
            if (log_Access == null)
            {
                return HttpNotFound();
            }
            return View(log_Access);
        }

        // POST: Log_Access/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [AuditFilter("访问日志删除", "64f145eb-ed67-41e3-acb4-d4295efbccc2")]
        public ActionResult DeleteConfirmed(long? id)
        {
            Log_Access log_Access = db.Log_Access.Find(id);
            db.Log_Access.Remove(log_Access);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // POST: Log_Access/BatchOperation/{operation}
        /// <summary>
        /// 批量操作
        /// </summary>
        /// <param name="operation">操作方法</param>
        /// <param name="ids">主键集合</param>
        /// <returns></returns>
        [HttpPost]
        [Route("Log_Access/BatchOperation/{operation}")]
        [AuditFilter("访问日志批量操作", "63d98e06-960a-4aa9-a284-13a6c6b77ac0")]
        public ActionResult BatchOperation(string operation, params long?[] ids)
        {
            var ajaxResponse = new AjaxResponse();
            if (ids.Length > 0)
            {
                try
                {
                    var models = db.Log_Access.Where(p => ids.Contains(p.Id)).ToList();
                    if (models.Count == 0)
                    {
                        ajaxResponse.Success = false;
                        ajaxResponse.Message = "没有找到匹配的项，项已被删除或不存在！";
                        return Json(ajaxResponse);
                    }
                    switch (operation.ToUpper())
                    {
                        case "DELETE":
                            #region 删除
                            {
                                db.Log_Access.RemoveRange(models);
                                db.SaveChanges();
                                ajaxResponse.Success = true;
                                ajaxResponse.Message = string.Format("已成功操作{0}项！", models.Count);
                                break;
                            }
                        #endregion
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    ajaxResponse.Success = false;
                    ajaxResponse.Message = ex.Message;
                }
            }
            else
            {
                ajaxResponse.Success = false;
                ajaxResponse.Message = "请至少选择一项！";
            }
            return Json(ajaxResponse);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
