﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Magicodes.WeiChat.Helpers
{
    /// <summary>
    /// 导出类型
    /// </summary>
    public enum ExportTypes
    {
        None = 0,
        Csv = 1,
        Excel = 2
    }
}