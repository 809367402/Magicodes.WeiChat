﻿using Magicodes.WeiChat.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Magicodes.WeiChat.Helpers
{
    public static class MvcHelper
    {
        /// <summary>
        /// 生成租户Url
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <returns></returns>
        public static string TenantAction(this UrlHelper Url, string actionName)
        {
            return Url.Action(actionName, new { TenantId = WeiChatApplicationContext.Current.TenantId });
        }

        public static string TenantAction(this UrlHelper Url, string actionName, string controllerName)
        {
            return Url.Action(actionName, controllerName, new { TenantId = WeiChatApplicationContext.Current.TenantId });
        }

        /// <summary>
        ///     生成租户Url
        /// </summary>
        /// <param name="Url">ASP.NET MVC 生成 URL的辅助对象</param>
        /// <param name="actionName">操作方法的名称</param>
        /// <param name="controllerName">控制器的名称</param>
        /// <param name="values">一个包含路由参数的对象。通过检查对象的属性，利用反射检索参数。该对象通常是使用对象初始值设定项语法创建的</param>
        /// <returns>租户Url</returns>
        public static string TenantAction(this UrlHelper Url, string actionName, string controllerName = null,
            object values = null)
        {
            return Url.TenantAction(actionName, controllerName, new RouteValueDictionary(values));
        }

        /// <summary>
        ///     生成租户Url
        /// </summary>
        /// <param name="Url">ASP.NET MVC 生成 URL的辅助对象</param>
        /// <param name="actionName">操作方法的名称。</param>
        /// <param name="controllerName">控制器的名称</param>
        /// <param name="dic">一个包含路由参数的对象</param>
        /// <returns>租户Url</returns>
        public static string TenantAction(this UrlHelper Url, string actionName, string controllerName = null,
            RouteValueDictionary dic = null)
        {
            if (dic == null)
            {
                dic = new RouteValueDictionary
                {
                    {"TenantId", WeiChatApplicationContext.Current.TenantId}
                };
            }
            else
            {
                if (!dic.ContainsKey("TenantId"))
                    dic["TenantId"] = WeiChatApplicationContext.Current.TenantId;
            }
            if ((actionName != null) && (controllerName == null))
                return Url.Action(actionName, dic);
            if ((actionName != null) && (controllerName != null))
                return Url.Action(actionName, controllerName, dic);
            return null;
        }
    }
}