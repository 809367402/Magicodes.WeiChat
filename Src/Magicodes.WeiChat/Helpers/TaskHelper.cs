﻿using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Data.Models.Site;
using Magicodes.WeiChat.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Magicodes.WeiChat.Helpers
{
    public class TaskHelper
    {
        public static void StartTask(WeiChat_SyncTypes type)
        {
            var tenantId = WeiChatApplicationContext.Current.TenantId;
            var userId = WeiChatApplicationContext.Current.UserId;
            var paramStr = tenantId + ";" + userId;
            var notifyInfo = new Site_Notify()
            {
                CreateBy = userId,
                CreateTime = DateTime.Now,
                IconCls = "fa fa-circle-o",
                UpdateTime = DateTime.Now,
                IsTaskFinish = false,
                Receiver = "Tenant_" + tenantId,
            };
            using (var db = new AppDbContext())
            {
                db.Site_Notifies.Add(notifyInfo);
                db.SaveChanges();
                WeiChatApplicationContext.Current.TaskManager.Start(type.ToString(), paramStr, notifyInfo);
            }
        }
    }
}