﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Magicodes.WeiChat.Helpers.UEEditor
{
    public class UploadResult
    {
        public UploadState State { get; set; }
        public string Url { get; set; }
        public string OriginFileName { get; set; }

        public string ErrorMessage { get; set; }
    }
}
