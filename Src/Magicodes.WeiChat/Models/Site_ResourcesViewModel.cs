﻿using Magicodes.WeiChat.Data.Models.Site;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Magicodes.WeiChat.Models
{
    public class GetJsonDataByMediaIdViewModel
    {
        public Site_FileBase FileBase { get; set; }
        public SiteResourceTypes ResourceType { get; set; }
    }
}