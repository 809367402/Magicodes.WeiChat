﻿using System;
using System.Collections.Generic;
using System.Linq;
using Magicodes.Logger.DebugLogger;
using Magicodes.WeiChat.Data;
using Magicodes.WeiChat.Data.Models;
using Magicodes.WeiChat.Infrastructure.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    public class TestData : WeChatEventDataBase
    {
        public WeiChat_User WeChatUser { get; set; }

        public string Test()
        {
            return "AAAA";
        }

        public override WeiChat_User GetWeChatUserByOpenId(string openId)
        {
            //测试
            return new WeiChat_User()
            {
                OpenId = "oXELNwzZgamuLS0JrJhVgdelzKyw",
                NickName = "雪雁"
            };
        }
    }

    [TestClass]
    public class EventTest : TestBase
    {
        public EventTest()
        {
            EventManager.Current.Logger = new DebugLogger("EventTest");
            //以下代码仅用于测试,请先在测试号维护好测试模板内容
            EventManager.Current.Listeners = new List<EventListener>()
            {
                new EventListener()
                {
                    Name=typeof(TestData).FullName,
                    DisplayName="测试事件",
                    Enabled=true,
                    Actions=new List<EventActionBase>()
                    {
                        //{{first.DATA}}
                        //会员昵称：{{keyword1.DATA}}
                        //关注时间：{{keyword2.DATA}}
                        //{{remark.DATA}}
                        new MessagesTemplateAction()
                        {
                            MessagesTemplateId ="riid7aad8OKRQD9Ey6dclWBBkrqZSFDhlpKh0_spGLA",
                            Items=new Dictionary<string, MessageTemplateValue>()
                            {
                                {"first",new MessageTemplateValue()
                                    {
                                        Expression="{\"感谢关注：\"+{0}.GetWeChatUserByOpenId({0}.WeChatUser.OpenId).NickName}"
                                    }
                                },
                                {"keyword1",new MessageTemplateValue()
                                    {
                                        Expression="{{0}.WeChatUser.NickName}"
                                    }
                                },
                                {"keyword2",new MessageTemplateValue()
                                    {
                                        Expression="{DateTime.Now.ToString(\"yyyy-MM-dd HH:mm:ss\")}"
                                    }
                                },
                                {"remark",new MessageTemplateValue()
                                    {
                                        Expression="{\"感谢您的关注\"+{0}.GetWeChatUserByOpenId({0}.WeChatUser.OpenId).NickName}"
                                    }
                                },
                            },
                            Receivers="{{0}.WeChatUser.OpenId}",
                            Url="http://xin-lai.com"
                        }
                    }
                }
            };
        }

        [TestMethod]
        public void TestTrigger()
        {
            EventManager.Current.Trigger(new TestData()
            {
                WeChatUser = new WeiChat_User()
                {
                    OpenId = "oXELNwzZgamuLS0JrJhVgdelzKyw",
                    NickName = "雪雁"
                }
            });
        }
    }
}
