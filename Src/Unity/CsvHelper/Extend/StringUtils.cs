﻿namespace CsvHelper.Extend
{
    public static class StringUtils
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsEmpty(this string text)
        {
            return string.IsNullOrWhiteSpace(text);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsNotEmpty(this string text)
        {
            return !text.IsEmpty();
        }
    }
}
